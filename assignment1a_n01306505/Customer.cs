﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment1a_n01306505
{
    public class Customer
    { 



            private string customerLName;
            private string customerFName;
            private string customerPhone;
            private string customerEmail;
            private string customerCountry;
            private string customerProv;
            private string customerAddress;
            private string customerCity;
            private string customerZip;

            public Customer()
            {

            }
           
           
            public string CustomerLName
            {
                get { return customerLName; }
                set { customerLName = value; }
            }
            public string CustomerFName
            {
                get { return customerFName; }
                set { customerFName = value; }
              }
        
            public string CustomerPhone
            {
                get { return customerPhone; }
                set { customerPhone = value; }
            }
            public string CustomerEmail
            {
                get { return customerEmail; }
                set { customerEmail = value; }
            }
            public string CustomerCountry
            {
                get { return customerCountry; }
                set { customerCountry = value; }
            }
            public string CustomerProv
            {
                get { return customerProv; }
                set { customerProv = value; }
            }

            public string CustomerAddress
            {
                get { return customerAddress; }
                set { customerAddress = value; }
            }

            public string CustomerCity
            {
                get { return customerCity; }
                set { customerCity = value; }
            }
            public string CustomerZip
            {
                get { return customerZip; }
                set { customerZip = value; }
            }



    }
    }