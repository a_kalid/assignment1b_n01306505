﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment1a_n01306505
{
    public class Billing
    {

        public Reservation reservation;         
        public Customer customer;
        public Hotel hotel;
        
            

        public Billing(Reservation r, Customer c, Hotel h )
        {
            reservation = r;
            customer = c;
            hotel = h;
            
            
            

        }

        public string PrintReservation()
        {
            string receipt = "Your Reservation:<br>";
            receipt += "Your total is :" + CalculateBill().ToString() + "<br/>";
            receipt += "First Name: " + customer.CustomerFName + "<br/>";
            receipt += "Last Name: " + customer.CustomerLName + "<br/>";
            receipt += "Email: " + customer.CustomerEmail + "<br/>";
            receipt += "Phone Number: " + customer.CustomerPhone + "<br/>";

            receipt += "Your Arrival Date " + reservation.arriveDate + "<br/>";
            receipt += "Your Departure Date " + reservation.departDate + "<br/>";
           

            //**receipt += "Total stay : " +( reservation.departDate-reservation.arriveDate);

            return receipt;
        }

        public double CalculateBill()
        {
            double total = 0;
            if (hotel.rooms == "one")
            { 

                total = 80;
            }
            else if (hotel.rooms == "two")
            {
                total = 100;
            }
            else if (hotel.rooms == "three" )
            {
                total = 120;
            }
            else if (hotel.rooms == "four")
            {
                total = 140;
            }
            else if (hotel.rooms == "five")
            {
                total = 200;
            }

            total +=hotel.kids.Count() * 1.5;

            return total;
        }

    }
}


