﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment1a_n01306505
{
    public class Reservation
    {


        public string arriveDate;
        public string departDate;
        public int reservAdults;
        public int reservKids;
        public int reservRooms;
        public string paymentType;
        public string reservExtra1;
        public string reservExtra2;
        public Reservation()
        {

        }
        public string ArriveDate
        {
            get { return arriveDate; }
            set { arriveDate = value; }
        }
        public string DepartDate
        {
            get { return departDate; }
            set { departDate = value; }
        }
        public int ReservAdults
        {
            get { return reservAdults; }
            set { reservAdults = value; }
        }
        public int ReservRooms
        {
            get { return reservRooms; }
            set { reservRooms = value; }
        }
        public int ReservKids
        {
            get { return reservKids; }
            set { reservKids = value; }
        }
        public string PaymentType
        {
            get { return paymentType; }
            set { paymentType = value; }
        }
        public string ReservExtra1
        {
            get { return reservExtra1; }
            set { reservExtra1 = value; }
        }
        public string ReserveExtra2
        {
            get { return reservExtra2; }
            set { reservExtra2 = value; }
        }



    }
}