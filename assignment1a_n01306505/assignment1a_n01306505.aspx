﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="assignment1a_n01306505.aspx.cs" Inherits="assignment1a_n01306505.assignment1a_n01306505" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Reservation</title>
</head>
<body>
    <form id="Reservform1" runat="server">
            <h2>Search for Hotels</h2>
            <div id="info" runat="server">

            </div>
            
            <asp:RadioButton runat="server" Text="Business" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Vacation" GroupName="via"/>
            <h3>Select your dates</h3>
            <h4>Check-in Date:</h4>
            <asp:TextBox runat="server" ID="arriveDate" placeholder="MM/DD/YYYY"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter arrival date" ControlToValidate="arriveDate" ID="validatorArrivalDate"></asp:RequiredFieldValidator>
            <br />
            <h4>Check-out Date:</h4>
            <asp:TextBox runat="server" ID="departDate" placeholder="MM/DD/YYYY"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter departure date" ControlToValidate="departDate" ID="validatordepartureDate"></asp:RequiredFieldValidator>
            <br />
            
            
            <asp:DropDownList runat="server" ID="reservRooms">
                <asp:ListItem Value="one" Text="1 Room"></asp:ListItem>
                <asp:ListItem Value="two" Text="2 Rooms"></asp:ListItem>
                <asp:ListItem Value="three" Text="3 Rooms"></asp:ListItem>
                <asp:ListItem Value="four" Text="4 Rooms"></asp:ListItem>
                <asp:ListItem Value="five" Text="5 Rooms"></asp:ListItem>
                
            </asp:DropDownList>
            <br />
            <asp:DropDownList runat="server" ID="reservAdults">
                <asp:ListItem Value="Guest1" Text="1-Adult"></asp:ListItem>
                <asp:ListItem Value="Guest2" Text="2-Adults"></asp:ListItem>
                <asp:ListItem Value="Guest3" Text="3-Adults"></asp:ListItem>
                <asp:ListItem Value="Guest4" Text="4-Adults"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:DropDownList runat="server" ID="reservKids">
                <asp:ListItem Value="Child0" Text="0-Children"></asp:ListItem>
                <asp:ListItem Value="Child1" Text="1-Child"></asp:ListItem>
                <asp:ListItem Value="Child2" Text="2-Children"></asp:ListItem>
                <asp:ListItem Value="Child3" Text="3-Children"></asp:ListItem>
                <asp:ListItem Value="Child4" Text="4-Children"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <h4>Reservation</h4>
            <asp:TextBox runat="server" ID="clientLName" placeholder="Last Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Last Name" ControlToValidate="clientLName" ID="validatorClientLName"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="clientFName" placeholder="First Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your First Name" ControlToValidate="clientFName" ID="validatorClientFName"></asp:RequiredFieldValidator>
            <br />
                                        
            <asp:TextBox runat="server" ID="clientPhone" placeholder="Phone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorClientPhone"></asp:RequiredFieldValidator>
            <br />
            <asp:CompareValidator runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="0000000000" ErrorMessage="This is an invalid phone number"></asp:CompareValidator>
            <br />
            <asp:TextBox runat="server" ID="clientEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="validatorCientEmail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="clientRegularExpressionValidator1" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
            <asp:DropDownList runat="server" ID="clientCountry">
                <asp:ListItem Value="Canada" Text="Canada"></asp:ListItem>
                <asp:ListItem Value="Denmark" Text="Denmark"></asp:ListItem>
                <asp:ListItem Value="Malaysia" Text="Malaysia"></asp:ListItem>
                <asp:ListItem Value="USA" Text="USA"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:TextBox runat="server" ID="clientProv" placeholder="State/Province"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientProv" ErrorMessage="Please enter State or Province"></asp:RequiredFieldValidator>
            <br />   

                                  
             <asp:TextBox runat="server" ID="clientAddress" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientAddress" ErrorMessage="Please enter an Address"></asp:RequiredFieldValidator>
            <br />   
            <asp:TextBox runat="server" ID="clientCity" placeholder="City"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientCity" ErrorMessage="Please enter City"></asp:RequiredFieldValidator>
            <br />  
            <asp:TextBox runat="server" ID="clientZip" placeholder="ZipCode"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientZip" ErrorMessage="Please enter City"></asp:RequiredFieldValidator>
            <br />   
            <div id="reservExtra1" runat="server">
            <asp:CheckBox runat="server" ID="reservCheckBox4" Text="Add a flight" />
            <asp:CheckBox runat="server" ID="reservCheckBox5" Text="Add a car" />
           
            </div>                     
            <div>
                <label>Choose One:</label><br />
            <asp:RadioButtonList runat="server" ID="paymentType">
                <asp:ListItem Text="Visa" >Visa</asp:ListItem>
                <asp:ListItem Text="Master-card" >Master-card</asp:ListItem>
            </asp:RadioButtonList>
            <asp:CustomValidator runat="server" ErrorMessage="Sorry we only accept Visa" ControlToValidate="paymentType" OnServerValidate="PaymentChoice_Validator"></asp:CustomValidator>
            <br />
            </div>
            <div id="reservExtra2" runat="server">
            <asp:CheckBox runat="server" ID="CheckBox1" Text="Text me my reservation" />
            <asp:CheckBox runat="server" ID="CheckBox2" Text="Get exclusive deals via email" />
            <asp:CheckBox runat="server" ID="CheckBox3" Text="Sign up for Free sightseeing Tour" />
            </div>

            
            


            
            <br /> 
            <div>
             <asp:Button ID="myButton" runat="server" OneClick="Book" Text="Submit" />
        </div>

        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <div runat="server" id="client"></div>
        <asp:ValidationSummary ID="ValidationSummary1" HeaderText="This is list of error" runat="server" />

       <div runat="server" id="BookingRes">
       </div>
       <div runat="server" id="OrderRes">
       </div>

            <footer runat="server" id="footer">


            </footer>
        
    </form>
</body>
</html>


